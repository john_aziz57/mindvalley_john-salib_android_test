package com.mindvalley.library.connection;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.URL;

/**
 * ImageTask return Bitmap Object to the ImageTask Listeners
 * Created by JSalib on 13-Jul-16.
 */
public class ImageTask extends Task {

    @Override
    public void execute() {
        InputStream in;
        BufferedInputStream buf;

        try {
            URL url = new URL(getUrl());
            in = url.openStream();
            buf = new BufferedInputStream(in);

            // Convert the BufferedInputStream to a Bitmap
            Bitmap bMap = BitmapFactory.decodeStream(buf);
            if (in != null) {
                in.close();
            }
            if (buf != null) {
                buf.close();
            }

            getTaskResult().addResult(bMap);
            getTaskResult().setResultType("Bitmap");
            this.setStatus(Task.SUCCESS);
//            return new BitmapDrawable(bMap);

        } catch (Exception e) {
            Log.e("Error reading file", e.toString());
            this.setStatus(Task.FAILURE);
        }
    }

}
