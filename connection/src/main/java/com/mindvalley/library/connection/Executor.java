package com.mindvalley.library.connection;

/**
 * Implement this interface incase you want to handle the tasks manually
 * Created by John on 15-Jul-16.
 */
public interface Executor {

    /**
     * Responsible for executing the task according to its type
     * , manging its threads and caching results if available
     * <p>
     * In case of you are not running the task thread itself, make sure that you handle the task back to
     * the task manager "TaskManager.taskToHandler(task);" in order for the task manager to cache the results
     * and remove the task from its storage
     * <p>
     * if you ran the thread , the task will handle itself to the taskManager
     *
     * @param task the task that is required to execute
     * @return id of the task
     */
    int execute(Task task);
}
