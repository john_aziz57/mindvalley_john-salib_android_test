package com.mindvalley.library.connection;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.HashMap;

/**
 * This Class is a show case of me using other libraries and how I can integrate it with
 * my Library
 * Created by JSalib on 14-Jul-16.
 */
public class PicassoTaskExecutor implements Executor {

    private final Context mContext;
    private final TaskManager mTaskManager;
    /* context for Picasso to use */
    /*Because Picasso holds weak reference to the Target, Target object will be removed by the GC
    * so I have to keep reference to them using this hashmap
    * https://github.com/square/picasso/issues/352 */
    private final HashMap<String, TaskTarget> referenceMap = new HashMap<>();

    public PicassoTaskExecutor(Context context, TaskManager taskManager) {
        mContext = context;
        mTaskManager = taskManager;
    }

    @Override
    public int execute(Task task) {

        final TaskTarget taskTarget = new TaskTarget(task) {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                getTask().setStatus(Task.SUCCESS);
                getTask().getTaskResult().addResult(bitmap);
                referenceMap.remove(getTask().getUrl());
                mTaskManager.taskToHandler(getTask());
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                getTask().setStatus(Task.FAILURE);
                getTask().getTaskResult().addResult(errorDrawable);
                referenceMap.remove(getTask().getUrl());
                mTaskManager.taskToHandler(getTask());
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };
        referenceMap.put(task.getUrl(), taskTarget);
        Picasso.with(mContext).load(task.getUrl()).into(taskTarget);

        return 0;
    }

    private static abstract class TaskTarget implements Target {
        private final Task mTask;

        public TaskTarget(Task task) {
            mTask = task;
        }

        public Task getTask() {
            return mTask;
        }
    }
}
