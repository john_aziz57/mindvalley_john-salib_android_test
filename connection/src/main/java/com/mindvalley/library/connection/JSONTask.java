package com.mindvalley.library.connection;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Created by JAnis on 14-Jul-16.
 */
public class JSONTask extends Task {
    @Override
    public void execute() {

        BufferedInputStream buf;

        try {
            URL url = new URL(getUrl());
            InputStream in = url.openStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            StringBuilder sb = new StringBuilder();
            String line;

            while ((line = br.readLine()) != null) {
                sb.append(line + "\n");
            }

            if (in != null) {
                in.close();
            }

            if (br != null) {
                br.close();
            }
            getTaskResult().addResult(sb.toString());
            getTaskResult().setResultType("JSON");
            this.setStatus(Task.SUCCESS);

        } catch (Exception e) {
            Log.e("Error reading file", e.toString());
            this.setStatus(Task.FAILURE);
        }
    }
}
