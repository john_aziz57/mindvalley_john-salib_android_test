package com.mindvalley.library.connection;

import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * This task executor keep track of the executed tasks and puts them in the pool for later use
 * Created by JSalib on 13-Jul-16.
 */
public class DefaultTaskExecutor implements Executor {
    ExecutorService mThreadPool = Executors.newCachedThreadPool();
    private int taskCounter = 0;
    private HashMap<Integer, Task> mTaskMap = new HashMap<>();


    public DefaultTaskExecutor() {

    }

    /**
     * @param task the task that will be executed
     * @return task id
     */
    @Override
    public int execute(Task task) {
        mTaskMap.put(++taskCounter, task);
        task.setId(taskCounter);
        mThreadPool.execute(task);

        return task.getId();
    }

    public Task requestTask(int id) {
        return mTaskMap.get(id);
    }


}
