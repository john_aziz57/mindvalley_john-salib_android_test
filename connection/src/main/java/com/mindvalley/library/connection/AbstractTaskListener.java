package com.mindvalley.library.connection;

/**
 * Created by John on 15-Jul-16.
 */
public abstract class AbstractTaskListener implements TaskListener {
    private int mId;

    @Override
    public void setId(int id) {
        mId = id;
    }

    @Override
    public int getId() {
        return mId;
    }
}
