package com.mindvalley.library.connection;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.LruCache;

import java.util.HashMap;

/**
 * This class receives that tasks and execute them, and should manage to return results in case of cached
 * result
 * This class is singleton
 * Created by JSalib on 13-Jul-16.
 */
public class TaskManager {
    public static final int IMAGE_TYPE = 0, JSON_TYPE = 1, OTHER_TYPE = 2;
    /* Bitmap Cache*/
    private static final int BITMAP_CACHE_SIZE = 10 * 1024 * 1024; // 10 MB
    /* Text or Text-like objects Cache*/
    private static final int TEXT_CACHE_SIZE = 10 * 1024 * 1024; // 10 MB
    /* General Cache*/
    private static final int GENERAL_CACHE_SIZE = 10 * 1024 * 1024; // 10 MB
    private static final TaskManager sTaskManager = new TaskManager();


    private DefaultTaskExecutor mDefaultTaskExecutor = new DefaultTaskExecutor();
    private Executor mImageExecutor = null;
    private Executor mJsonExecutor = null;
    private Executor mOtherExecutor = null;
    private HashMap<String, Task> mTaskHashMap = new HashMap<>();
    private Handler mHandler;

    private LruCache<String, Bitmap> mBitmapLruCache = new LruCache<String, Bitmap>(BITMAP_CACHE_SIZE) {
        @Override
        protected int sizeOf(String key, Bitmap value) {
            return value.getByteCount();
        }
    };
    private LruCache<String, String> mTextLruCache = new LruCache<String, String>(TEXT_CACHE_SIZE) {
        @Override
        protected int sizeOf(String key, String value) {
            return value.length();
        }
    };
    private LruCache<String, Object> mGeneralLruCache = new LruCache<>(GENERAL_CACHE_SIZE);


    //TODO move the task listeners to the TaskManager

    /**
     * private Constructor singleton pattern
     */
    private TaskManager() {
        mHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                Task task = (Task) msg.obj;
                switch (msg.what) {
                    case Task.SUCCESS:
                        cacheTask(task);
                        task.onSuccess();
                        break;
                    default:
                        task.onFailure();
                        break;
                }
                mTaskHashMap.remove(task.getUrl());
            }
        };
    }

    public static TaskManager getTaskManager() {
        return sTaskManager;
    }

    /**
     * In case you want to implement your own Executor, ie Picasso
     * you have to register this to the TaskManager and tell the TaskManager
     * which tasks it is responsible about
     * There are Three Types
     * ImageExecutor will handle Image
     * JSONExecutor will handle JSON files
     * OtherExecutor will handle All other request
     * <p>
     * In all of the above are nulls the DefaultTaskExecutor will handle this task
     * <p>
     * Example,
     * if you created an executor that can handle all image requests,
     * then you regisiter this executor to the TaskManager and all the ImageTasks wil
     * be handled down to your image executor
     * <p>
     * Unfortunately Now I support one executor for each type, will change
     * to it, so that the user can define his own executor with his own task type and so support multiple
     * executors
     *
     * @param executor Instance of Executor Interface
     * @param taskType The type of tasks the it will handle
     */
    public void registerExecutor(Executor executor, int taskType) {
        switch (taskType) {
            case IMAGE_TYPE:
                mImageExecutor = executor;
                return;
            case JSON_TYPE:
                mJsonExecutor = executor;
                return;
            case OTHER_TYPE:
                mOtherExecutor = executor;
                return;
        }
    }

    /**
     * general purpose new task, handles the url and choose the most suitable task for its execution
     *
     * @param url request URL
     */
    public synchronized void addTask(String url, TaskListener taskListener) {
        //TODO determine the type if possible
        // then call addTask(String url, int requestType, TaskListener listener)
    }

    /**
     * The preferred way to request a task is to send it ot the TaskManager and
     * send the listener to wait for the results
     *
     * @param url         requested URL
     * @param requestType Image, JSON, .. etc
     * @param listener    Listener will be called when the task ends
     */
    public synchronized void addTask(String url, int requestType, TaskListener listener) {
        TaskResult result = getCache(url, requestType);
        if (result.getResult() != null) {
            sendResultToListener(result, listener);
            return;
        }
        /*Check if task already in queue*/
        Task oldTask = mTaskHashMap.get(url);
        if (oldTask != null) {/* if yes */
            oldTask.addListener(listener); /* add that listener to it*/
            return;
        }
        /*if no, create new task and send it to executor*/
        Task task = TaskFactory.createTask(this, url, requestType, listener);
        sendTaskToExecutor(task);
    }


    /**
     * add your own defined task to the ExecutionPool,
     * if this task's result is cached before the result will be returned
     * to the listener otherwise  it will be passed down to the
     * suitable executor
     * <p>
     * Adding your own task, means that it will execute even if there is
     * a task with the same url waiting to be executed
     *
     * @param task user defined task
     */
    public synchronized void addTask(Task task) {
        TaskResult result = getCache(task.getUrl(), task.getTaskType());
        if (result.getResult() != null) {
            task.setTaskResult(result);
            task.onSuccess();
            return;
        }

        sendTaskToExecutor(task);
    }

    /**
     * cancel the task, takes listener id
     * if no other listener is waiting for the task it will be cancelled
     * otherwise this listener will be only removed from the task
     * and the task will continue to execute normally
     *
     * @param url        the task's url
     * @param listenerId the id of the listener that is registered to this task.
     *                   to get the id TaskListener.getId();
     * @return true if listener was removed successfully, else return false
     */
    public synchronized boolean cancelTask(String url, int listenerId) {
        try {
            Task task = mTaskHashMap.get(url);
            return task.removeListener(listenerId);
        } catch (Exception e) {
            return false;
        }
    }


    /**
     * This method clean all the cache whether bitmap,Json,general cache
     */
    public void cleanCache(){
        mBitmapLruCache.evictAll();
        mTextLruCache.evictAll();
        mGeneralLruCache.evictAll();
    }
    /**
     * send the task for its default executor according to its type
     *
     * @param task task to be executed
     */
    private void sendTaskToExecutor(Task task) {
        if (mImageExecutor != null && task.getTaskType() == IMAGE_TYPE) {
            mImageExecutor.execute(task);
        } else if (mJsonExecutor != null && task.getTaskType() == JSON_TYPE) {
            mJsonExecutor.execute(task);
        } else if (mOtherExecutor != null) {
            mOtherExecutor.execute(task);
        } else {
            mDefaultTaskExecutor.execute(task);
        }
    }

    /**
     * Takes the tasks after being executed
     * and send them to the handler
     *
     * @param task task that will be sent to the handler
     */
    protected synchronized void taskToHandler(Task task) {
        Message completeMessage = mHandler.obtainMessage(task.getStatus(), task);
        completeMessage.sendToTarget();
    }

    /**
     * cache the task results according to its type
     *
     * @param task task after being executed
     */
    private void cacheTask(Task task) {
        switch (task.getTaskType()) {
            case TaskManager.IMAGE_TYPE:
                mBitmapLruCache.put(task.getUrl(), (Bitmap) task.getTaskResult().getResult());
                break;
            case TaskManager.JSON_TYPE:
                mTextLruCache.put(task.getUrl(), (String) task.getTaskResult().getResult());
                break;
            default:
                mGeneralLruCache.put(task.getUrl(), task.getTaskResult().getResult());
                break;
        }
    }

    /**
     * used when there is no task created yet and we have cached value and that value
     * is needed to be sent to the listeners
     *
     * @param result   the result that will be sent to the listener
     * @param listener the listener that will receive the result
     */
    private void sendResultToListener(TaskResult result, TaskListener listener) {
        listener.onSucceed(result);
    }

    /**
     * Return the task result if TaskResult.getResult() equals null, then there is
     * no cached values for this url.
     *
     * @param url      task's url
     * @param taskType IMAGE_TYPE, JSON_TYPE
     * @return cached value inside TaskResult object
     */
    private TaskResult getCache(String url, int taskType) {
        TaskResult result = new TaskResult();

        /* if cache is found this means that this result is successful set the result status accordingly*/
        result.setStatus(TaskResult.SUCCESS);
        switch (taskType) {
            case TaskManager.IMAGE_TYPE:
                result.addResult(mBitmapLruCache.get(url));
                break;
            case TaskManager.JSON_TYPE:
                result.addResult(mTextLruCache.get(url));
                break;
            default:
                result.addResult(mGeneralLruCache.get(url));
                break;
        }
        return result;
    }
}
