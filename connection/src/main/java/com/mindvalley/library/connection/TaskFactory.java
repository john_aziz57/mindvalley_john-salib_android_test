package com.mindvalley.library.connection;

/**
 * Responsible for creating the task according to its type and
 * set other required attributes like the url,the listener and type
 * Created by JSalib on 13-Jul-16.
 */
public class TaskFactory {
    //TODO add task id here
    public static Task createTask(TaskManager taskManager, String url, int taskType, TaskListener listener) {
        Task task;
        switch (taskType) {
            case TaskManager.IMAGE_TYPE:
                task = new ImageTask();
                break;
            case TaskManager.JSON_TYPE:
                task = new JSONTask();
                break;
            default:
                //TODO implement the default task
                return null;
        }

        task.setUrl(url);
        task.setTaskType(taskType);
        task.addListener(listener);
        task.setTaskManager(taskManager);
        return task;
    }
}
