package com.mindvalley.library.connection;

/**
 * The listener the will be waiting for the thread to finish, either successfully or else
 * Created by JSalib on 13-Jul-16.
 */
public interface TaskListener {

    void onSucceed(TaskResult result);

    void onFailure(TaskResult reuslt);

    /**
     * sets the id of the TaskListener.
     * The id is used to cancel tasks and remove the listener from the task
     *
     * @param id id of the listener
     */
    void setId(int id);

    /**
     * returns the same id that was set by setId method
     * The id is used to cancel tasks and remove the listener from the task
     *
     * @return id of the listener
     */
    int getId();
}
