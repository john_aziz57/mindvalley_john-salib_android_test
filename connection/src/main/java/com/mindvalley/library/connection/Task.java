package com.mindvalley.library.connection;

import java.util.HashMap;

/**
 * Holds the primary data a task could need like the status and result and listeners
 * this is the base class for any task,
 * <p>
 * the Execute method will always be executed in a different thread other than the UI thread, in case you have used
 * the default executor, otherwise your executor should handle running the task in different thread.
 * <p>
 * Task already implements the Runnable class so, you should be able to run it in a different thread
 * <p>
 * override the execute method to create your own code that will run in a different thread
 * Created by JSalib on 13-Jul-16.
 */
public abstract class Task implements Runnable {
    public static final int NOT_STARTED = 0, PENDING = 1, SUCCESS = 2, FAILURE = 3, CANCELED = 4;

    private TaskManager mTaskManager;
    private int mId = -1;
    private String mUrl = "";
    private Thread mCurrentThread;
    //    private ArrayList<TaskListener> mListeners = new ArrayList<>();
    private HashMap<Integer, TaskListener> mListenersMap = new HashMap<>();
    private TaskResult mTaskResult = new TaskResult();
    private int mTaskStatus = NOT_STARTED;
    private int mTaskType = 0;
    private int mListenerCounter = 0;

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public synchronized int getStatus() {
        return mTaskStatus;
    }

    public synchronized void setStatus(int status) {
        mTaskStatus = status;
    }

    public int getTaskType() {
        return mTaskType;
    }

    public void setTaskType(int mTaskType) {
        this.mTaskType = mTaskType;
    }


    public synchronized int addListener(TaskListener listener) {
        int listenerId = ++mListenerCounter;
        mListenersMap.put(listenerId, listener);
        listener.setId(listenerId);
        return listenerId;//TODO return listener Id
    }

    public TaskResult getTaskResult() {
        return mTaskResult;
    }

    public void setTaskResult(TaskResult result) {
        mTaskResult = result;
    }

    public void setTaskManager(TaskManager taskManager) {
        mTaskManager = taskManager;
    }


    public abstract void execute();// set at the end whether the task status succeeded or not

    public void onSuccess() {
        mTaskResult.setStatus(TaskResult.SUCCESS);
        for (TaskListener listener : mListenersMap.values())
            listener.onSucceed(mTaskResult);
    }

    public void onFailure() {
        mTaskResult.setStatus(TaskResult.FAILURE);
        for (TaskListener listener : mListenersMap.values())
            listener.onFailure(mTaskResult);
    }


    /**
     * remove the listener from the listeners registered to this task
     * after removing the listener if there is no other listener
     * this task will cancelled or interrupted if it has already started
     *
     * @param listenerId the id of the listener
     * @return true if managed to remove listener, false otherwise
     */
    public boolean removeListener(int listenerId) {
        //TODO check if this condition is even useful , probably not
        /* can't remove the task in case of success or failure because task is looping over
        * those listener to send them the result*/
        if (getStatus() == SUCCESS || getStatus() == FAILURE)
            return false;

        mListenersMap.remove(listenerId);
        if (mListenersMap.size() == 0) {
            if (getStatus() == Task.PENDING)
                mCurrentThread.interrupt();
            else
                setStatus(Task.CANCELED);
        }
        return true;
    }


    @Override
    public void run() {
        if (getStatus() != CANCELED) {
            setStatus(Task.PENDING);
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
            mCurrentThread = Thread.currentThread();
            execute();
            mTaskManager.taskToHandler(this);
        }
    }
}
