package com.mindvalley.library.connection;

import java.util.HashMap;

/**
 * Created by JAnis on 13-Jul-16.
 */
public class TaskResult {
    public final static int SUCCESS = 0, FAILURE = 1, NO_RESULT = 2;
    public final static String RESULT = "RESULT";


    private String mResultType = "";
    private int mStatus = NO_RESULT;

    HashMap<String, Object> mMap = new HashMap<>();

    public void addObject(String key, Object object) {
        mMap.put(key, object);
    }

    public void addResult(Object object) {
        mMap.put(RESULT, object);
    }

    public Object getResult() {
        return mMap.get(RESULT);
    }

    public Object getResult(String key) {
        return mMap.get(key);
    }

    public boolean isSuccess() {
        return mStatus == SUCCESS;
    }

    public void setStatus(int status) {
        mStatus = status;
    }

    public int getStatus() {
        return mStatus;
    }

    public String getResultType() {
        return mResultType;
    }

    public void setResultType(String mResultType) {
        this.mResultType = mResultType;
    }

}
