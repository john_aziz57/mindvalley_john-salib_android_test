package com.mindvalley.mindvalley_johnsalib_android_test;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
//import android.support.v4.widget.SwipeRefreshLayout;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mindvalley.library.connection.AbstractTaskListener;
import com.mindvalley.library.connection.PicassoTaskExecutor;
import com.mindvalley.library.connection.TaskListener;
import com.mindvalley.library.connection.TaskManager;
import com.mindvalley.library.connection.TaskResult;



/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    private EditText editText;
    private Button mRunSampleButton,mGetImageButton;
    private LinearLayout mLinearLayout;
//    private SwipeRefreshLayout swipeContainer;


    private TaskListener taskListener;
    private TaskManager mTaskManager;
    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        mTaskManager = TaskManager.getTaskManager();

        PicassoTaskExecutor picassoTaskExecutor = new PicassoTaskExecutor(getActivity(), mTaskManager);
        mTaskManager.registerExecutor(picassoTaskExecutor, TaskManager.IMAGE_TYPE);

        View v = inflater.inflate(R.layout.fragment_main, container, false);

        mLinearLayout = (LinearLayout)v.findViewById(R.id.result_linearLayout);
        editText = (EditText) v.findViewById(R.id.url_editText);
        mRunSampleButton = (Button) v.findViewById(R.id.run_sample_button);
        mRunSampleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String [] urls = new String [] {
                        "http://www.ew.com/sites/default/files/i/2015/12/02/chadwick-boseman-as-black-panther.jpg",
                        "http://dummyimage.com/100x100/000100/ffffff",
                        "http://dummyimage.com/200x200/000100/ffffff",
                        "http://dummyimage.com/300x300/000100/ffffff",
                        "http://dummyimage.com/500x500/000100/ffffff",
                        "http://dummyimage.com/500x600/000100/ffffff",
                        "http://dummyimage.com/1500x1500/101100/ffffff",
                        "http://dummyimage.com/2500x2500/101100/ffffff"
                };

                for (String url : urls) {
                    mTaskManager.addTask(url,
                            TaskManager.IMAGE_TYPE,
                            new AbstractTaskListener() {
                                @Override
                                public void onSucceed(TaskResult result) {
                                    ImageView imageView = new ImageView(getActivity());
                                    TextView textView = new TextView(getActivity());

                                    Bitmap bitmap = (Bitmap) result.getResult();
                                    imageView.setImageDrawable(new BitmapDrawable(getResources(), bitmap));

                                    textView.setText(bitmap.getWidth() + " " + bitmap.getHeight());
                                    mLinearLayout.addView(textView);
                                    mLinearLayout.addView(imageView);
                                }

                                @Override
                                public void onFailure(TaskResult result) {
                                    Toast.makeText(getActivity(), "Failure", Toast.LENGTH_LONG).show();
                                }
                            });
                }

                mTaskManager.addTask("http://pastebin.com/raw/wgkJgazE",
                        TaskManager.JSON_TYPE,
                        new AbstractTaskListener() {
                            @Override
                            public void onSucceed(TaskResult result) {
                                TextView textView = new TextView(getActivity());
                                String str = (String) result.getResult();
                                textView.setText(str);
                                textView.setMaxLines(10);
                                textView.setVerticalScrollBarEnabled(true);
                                textView.setHorizontallyScrolling(true);
                                textView.setMovementMethod(new ScrollingMovementMethod());
                                mLinearLayout.addView(textView);
                            }

                            @Override
                            public void onFailure(TaskResult result) {
                                Toast.makeText(getActivity(), "Failure JSON", Toast.LENGTH_LONG).show();
                            }
                        });


            }
        });
        mGetImageButton = (Button)v.findViewById(R.id.get_image_button);
        mGetImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTaskManager.addTask(editText.getText().toString(),
                        TaskManager.IMAGE_TYPE,
                        new AbstractTaskListener() {
                            @Override
                            public void onSucceed(TaskResult result) {
                                ImageView imageView = new ImageView(getActivity());
                                imageView.setImageDrawable(new BitmapDrawable(getResources(), (Bitmap)result.getResult()));
                                mLinearLayout.addView(imageView);
                            }

                            @Override
                            public void onFailure(TaskResult result) {
                                Toast.makeText(getActivity(), "Failure", Toast.LENGTH_LONG).show();
                            }
                        });
            }
        });

        //TODO pull to refresh feature
//        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        // Setup refresh listener which triggers new data loading
//        swipeContainer.setOnRefreshListener(new OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                // Your code to refresh the list here.
//                // Make sure you call swipeContainer.setRefreshing(false)
//                // once the network request has completed successfully.
//                fetchTimelineAsync(0);
//            }
//        });
        // Configure the refreshing colors
//        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
//                android.R.color.holo_green_light,
//                android.R.color.holo_orange_light,
//                android.R.color.holo_red_light);


        return v;
    }

}
